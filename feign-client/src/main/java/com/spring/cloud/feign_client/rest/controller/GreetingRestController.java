package com.spring.cloud.feign_client.rest.controller;

import com.spring.cloud.feign_client.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingRestController {

    @Autowired
    private GreetingService greetingService;

    @GetMapping("/greeting")
    public String greeting() {
        return greetingService.greeting();
    }

}
