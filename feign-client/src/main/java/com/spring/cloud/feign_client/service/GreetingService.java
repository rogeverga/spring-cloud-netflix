package com.spring.cloud.feign_client.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("greeting-service")
public interface GreetingService {

    @GetMapping("/greeting")
    String greeting();

}
