package com.spring.cloud.greeting_service.rest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingRestController {

    @GetMapping("/greeting")
    public String greeting() {
        return "Hello!";
    }

}
